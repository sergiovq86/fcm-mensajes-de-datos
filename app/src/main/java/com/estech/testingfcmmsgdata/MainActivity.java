package com.estech.testingfcmmsgdata;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.estech.testingfcmmsgdata.retrofit.MyClient;
import com.estech.testingfcmmsgdata.retrofit.MyNotification;
import com.estech.testingfcmmsgdata.retrofit.NotifData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public final static String ACTION_DATA_AVAILABLE = "com.estech.MSG_AVAILABLE";

    private TextInputEditText etToken, etMsg;
    private ImageButton btSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Método para crear el canal de notificaciones
        createNotificationChannel();

        // Si se pulsa sobre una notificación, aquí se recuperan los datos extra mandados a
        //través del intent
        if (getIntent().getExtras() != null) {
            String nombre = getIntent().getExtras().getString("fecha");
            Toast.makeText(this, nombre, Toast.LENGTH_SHORT).show();
        }

        Button logTokenButton = findViewById(R.id.logTokenButton);
        logTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "getInstanceId failed", task.getException());
                                    return;
                                }

                                // Get new Instance ID token
                                String token = task.getResult().getToken();

                                // Log and toast
                                String msg = getString(R.string.msg_token_fmt, token);
                                Log.d(TAG, msg);
                                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        btSend = findViewById(R.id.bt_send);
        etMsg = findViewById(R.id.et_msg);
        etToken = findViewById(R.id.et_token);

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String token = etToken.getText().toString();
                String msg = etMsg.getText().toString();
                if (!token.isEmpty() && !msg.isEmpty()) {
                    sendNotification(token, msg);
                }
            }
        });
    }

    private void sendNotification(String token, String msg) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        String fecha = simpleDateFormat.format(new Date());

        NotifData notifData = new NotifData(msg, "My App Notification", fecha);
        MyNotification notif = new MyNotification(token, "Mensajes_disponibles", notifData);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyClient myClient = retrofit.create(MyClient.class);
        Call<Object> objectCall = myClient.sendNotification(notif);
        objectCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Object object = response.body();
//                    try {
//                        double success = object.getDouble("success");
//                        if (success == 1.0) {
//                            Toast.makeText(MainActivity.this, "Mensaje enviado correctamente", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(MainActivity.this, "Error enviando el mensaje", Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (JSONException e) {
//                        Toast.makeText(MainActivity.this, "Error enviando el mensaje", Toast.LENGTH_SHORT).show();
//                        e.printStackTrace();
//                    }
                } else {
                    Toast.makeText(MainActivity.this, "Error enviando el mensaje", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error enviando el mensaje: " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channel_id = getString(R.string.default_notification_channel_name);
            CharSequence channel_name = getString(R.string.default_notification_channel_name);
            String channel_description = getString(R.string.default_notification_channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(channel_id, channel_name, importance);
            channel.setDescription(channel_description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
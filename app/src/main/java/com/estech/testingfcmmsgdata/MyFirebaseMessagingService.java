package com.estech.testingfcmmsgdata;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Método llamado cuando se recibe un mensaje
     *
     * @param remoteMessage Objeto que representa el mensaje recibido de FCM
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        /* Hya dos tipos de mensajes: mensajes de dataos y mensajes de notificación.
        Los mensajes de datos se pueden gestionar aquí si la app está en background o foreground.
        Los mensajes de notificación son recibidos aquí en foreground. Si la App está en background
        se autogenera una n otificación.
         */

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // comprueba si el mensaje es de datos
        if (remoteMessage.getData().size() > 0) {

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> datos = remoteMessage.getData();
            String titulo = datos.get("title");
            String body = datos.get("body");
            String fecha = datos.get("fecha");
            sendNotification(titulo, body, fecha);
        }

        // Compruebab si el mensaje es una notificación
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    /**
     * Llamado cuando el token es refrescado
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        sendRegistrationToServer(token);
    }
    // [END on_new_token]


    /**
     * Método para mandar el token al servidor
     *
     * @param token nuevo token
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Notificación simple
     *
     * @param titulo titulo a mostrar
     * @param body   cuerpo del mensaje recibido en el FCM
     */
    private void sendNotification(String titulo, String body, String fecha) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("fecha", fecha);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channel_id = getString(R.string.default_notification_channel_name);
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channel_id)
                        .setSmallIcon(R.drawable.ic_chat)
                        .setContentTitle(titulo)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(943, notificationBuilder.build());

    }

}

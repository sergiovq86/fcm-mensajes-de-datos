package com.estech.testingfcmmsgdata.retrofit;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface MyClient {

    String BASE_URL = "https://fcm.googleapis.com/";
    String SERVER_TOKEN = "AAAACPlFXgY:APA91bGpW5E_UMz7WYhmbY_FeQCsULADdTFhp1UfRSb_naiBsnfAMtUCU0QeoC7ekaA_4y45FtnM5nBKR9ErpGa0H9vGcl9toIAW_KFlbrm2waj_QqEyh0KNHP4x9ood2JGLB7ZiL-Af";

    @Headers({"Authorization:key=" + SERVER_TOKEN, "Content-Type:application/json"})
    @POST("fcm/send")
    Call<Object> sendNotification(@Body MyNotification mynotif);
}

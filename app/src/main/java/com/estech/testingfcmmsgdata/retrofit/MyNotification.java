package com.estech.testingfcmmsgdata.retrofit;

public class MyNotification {

    private String to, collapse_key;
    private NotifData data;

    public MyNotification(String to, String collapse_key, NotifData data) {
        this.to = to;
        this.collapse_key = collapse_key;
        this.data = data;
    }
}

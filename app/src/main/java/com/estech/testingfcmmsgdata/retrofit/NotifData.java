package com.estech.testingfcmmsgdata.retrofit;

public class NotifData {

    private String body, title, fecha;

    public NotifData(String body, String title, String fecha) {
        this.body = body;
        this.title = title;
        this.fecha = fecha;
    }
}
